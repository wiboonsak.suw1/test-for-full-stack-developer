<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Developer_Exam extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Developer_Exam_model');
    }

    public function index() {
        $this->load->view('Developer_Exam_Detail');
    }

    //-------------------------------------------------------------
    public function changePattern1() {
        $Pattern1 = $this->input->post('Pattern1');
        $text = '';
        if($Pattern1==1){
            $text = 'X';
        }else if($Pattern1==2){
            $text = "XOX<br>OXO<br>XOX";
        }else if($Pattern1==3){
            $text = "XOXOX<br>OXOXO<br>OOXOO<br>OXOXO<br>XOXOX";
        }else if($Pattern1==4){
            $text = "XOXOXOX<br>OXOXOXO<br>OOXOXOO<br>OOOXOOO<br>OOXOXOO<br>OXOXOXO<br>XOXOXOX";
        }else if($Pattern1==5){
            $text = "XOXOXOXOX<br>OXOXOXOXO<br>OOXOXOXOO<br>OOOXOXOOO<br>OOOOXOOOO<br>OOOXOXOOO<br>OOXOXOXOO<br>OXOXOXOXO<br>XOXOXOXOX";
        }        
        echo $text;
        }
    //-------------------------------------------------------------
    public function changePattern2() {
        $Pattern2 = $this->input->post('Pattern2');
        $text = '';
        if($Pattern2==1){
            $text = 'X';
        }else if($Pattern2==2){
            $text = "XX<br>XX";
        }else if($Pattern2==3){
            $text = "X X<br>XXX<br>X X";
        }else if($Pattern2==4){
            $text = "X  X<br>XXXX<br>XXXX<br>X  X";
        }else if($Pattern2==5){
            $text = "X   X<br>XX XX<br>XXXXX<br>XX XX<br>X   X";
        }        
        echo $text;
        }
    //-------------------------------------------------------------
    public function changePattern3() {
        $Pattern3 = $this->input->post('Pattern3');
        $text = '';
        if($Pattern3==1){
            $text = 'X';
        }else if($Pattern3==2){
            $text = "XX<br>XX";
        }else if($Pattern3==3){
            $text = "XXX<br><span style='color:red'>YY</span>X<br>XXX";
        }else if($Pattern3==4){
            $text = "XXXX<br><span style='color:red'>YYY</span>X<br>XX<span style='color:red'>Y</span>X<br>XXXX";
        }else if($Pattern3==5){
            $text = "XXXXX<br><span style='color:red'>YYYY</span>X<br>XXX<span style='color:red'>Y</span>X<br>X<span style='color:red'>YYY</span>X<br>XXXXX";
        }else if($Pattern3==6){
            $text = "XXXXXX<br><span style='color:red'>YYYYY</span>X<br>XXXX<span style='color:red'>Y</span>X<br>X<span style='color:red'>Y</span>XX<span style='color:red'>Y</span>X<br>X<span style='color:red'>YYYY</span>X<br>XXXXXX";
        }else if($Pattern3==7){
            $text = "XXXXXXX<br><span style='color:red'>YYYYYY</span>X<br>XXXXX<span style='color:red'>Y</span>X<br>X<span style='color:red'>YYY</span>X<span style='color:red'>Y</span>X<br>X<span style='color:red'>Y</span>XXX<span style='color:red'>Y</span>X<br>X<span style='color:red'>YYYYY</span>X<br>YXXXXXX";
        }        
        echo $text;
        }

    //-------------------------------------------------------------
    public function datetothai() {
        $dateinput = $this->input->post('dateinput');
        $GetthaiDate = $this->Developer_Exam_model->GetthaiDate($dateinput);
        echo $GetthaiDate;
    }

    //-------------------------------------------------------------
    public function searchdata() {
        $name = $this->input->post('name');
        $searchdata = "https://jsonplaceholder.typicode.com/users?name_like=" . $name;
        echo $searchdata;
    }

}
