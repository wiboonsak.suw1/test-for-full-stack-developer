<?php

class Developer_Exam_model extends CI_Model {

    //---------------------------  
    function GetthaiDate($day) {
        $d_array = array(
            "Sunday" => "อาทิตย์",
            "Monday" => "จันทร์",
            "Tuesday" => "อังคาร",
            "Wednesday" => "พุธ",
            "Thursday" => "พฤหัสบดี",
            "Friday" => "ศุกร์",
            "Saturday" => "เสาร์"
        );
        $dateformat = $this->formatdate($day);
        $dateArray = explode("/", $day);
        $date = $dateArray[0];
        $mon = $dateArray[1];
        $year = $dateArray[2] + 543;
        $monthArray = array("01" => "มกราคม", "02" => "กุมภาพันธ์", "03" => "มีนาคม", "04" => "เมษายน", "05" => "พฤษภาคม", "06" => "มิถุนายน", "07" => "กรกฏาคม", "08" => "สิงหาคม", "09" => "กันยายน", "10" => "ตุลาคม", "11" => "พฤศจิกายน", "12" => "ธันวาคม");
        if ($date < 10) {
            $date = str_replace("0", "", $date);
        }
        return "วัน" . $d_array[date('l', strtotime($dateformat))] . "ที่ " . $date . " " . $monthArray[$mon] . " " . $year;
    }

    //---------------------------	 
    function formatdate($date1) {

        $dateArray = explode("/", $date1);
        $date2 = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
        return $date2;
    }

    //---------------------------	 
    function searchdata($name) {
        $showdata = "https://jsonplaceholder.typicode.com/users?name_like=" . $name;
        return $showdata;
    }

}
