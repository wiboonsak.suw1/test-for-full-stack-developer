<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<title>Highdmin - Responsive Bootstrap 4 Admin Dashboard</title>-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <!-- X editable -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
            @media only screen and (max-width: 600px) {
                img {
                    width: 350px;
                    height: 250px;
                }
            }
        </style>
    </head>

    <body>

        <div class="jumbotron text-center">
            <h2>Test for Full-Stack Developer</h2>
        </div>

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h5>1. Develop an application for given input data and output as following.</h5>
                    <h5>1.1 Pattern on below</h5>
                    <div class="row">
                        <label class="col-xs-3 col-sm-1" style="text-align:right;padding-top: 5px">
                            n = 
                        </label>
                        <div class="col-xs-9 col-sm-5">
                           <input class="form-control" type="number" id="Pattern1" name="Pattern1" onchange="changePattern1(this.value)">
                        </div>
                        <div class="col-xs-12 col-sm-12" style="padding-top: 5px">
                            <p id="showPattern" ></p>
                        </div>

                    </div>
                    <h5>1.2 Pattern on below</h5>
                    <div class="row">
                        <label class="col-xs-3 col-sm-1" style="text-align:right;padding-top: 5px">
                            n = 
                        </label>
                        <div class="col-xs-9 col-sm-5">
                           <input class="form-control" type="number" id="Pattern2" name="Pattern2" onchange="changePattern2(this.value)">
                        </div>
                        <div class="col-xs-12 col-sm-12" style="padding-top: 5px">
                            <p id="showPattern2" ></p>
                        </div>

                    </div>
                    <h5>1.3 Pattern on below</h5>
                    <div class="row">
                        <label class="col-xs-3 col-sm-1" style="text-align:right;padding-top: 5px">
                            n = 
                        </label>
                        <div class="col-xs-9 col-sm-5">
                           <input class="form-control" type="number" id="Pattern3" name="Pattern3" onchange="changePattern3(this.value)">
                        </div>
                        <div class="col-xs-12 col-sm-12" style="padding-top: 5px">
                            <p id="showPattern3" ></p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5>2. Develop an application for date input (day/month/year) without using Library and Built-in Function (i.e. moment() and new Date() are not allowed).</h5>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <input class="form-control" type="text" id="dateinput" name="dateinput" onchange="datetothai(this.value)" placeholder="ex.6/01/2021">
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            Date Thai <span id="datethai"></span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5>3. Create a page for display and searching users by using API from “https://jsonplaceholder.typicode.com/users” Response data must be in JSON format only.</h5>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <input class="form-control" type="text" id="searchdata" name="searchdata" onchange="searchdata(this.value)" placeholder="Please Enter Name Suename">
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div id="showdata"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5>4. In September The customer want to build online shopping application and assign this project to you. About features and spec of the shop is up to you. Please design database and make us WOW when present to us how awesome idea you have. </h5>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <img src="img/Database.jpg" alt="Girl in a jacket" >
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div>
                                <p>Database: `shop`</p>
                                <p>Table structure for table `cart`</p>
                                <p>CREATE TABLE `cart` (
                                    `id` bigint(20) NOT NULL,
                                    `userId` bigint(20) DEFAULT NULL,
                                    `sessionId` varchar(100) NOT NULL,
                                    `token` varchar(100) NOT NULL,
                                    `status` smallint(6) NOT NULL DEFAULT 0,
                                    `firstName` varchar(50) DEFAULT NULL,
                                    `middleName` varchar(50) DEFAULT NULL,
                                    `lastName` varchar(50) DEFAULT NULL,
                                    `mobile` varchar(15) DEFAULT NULL,
                                    `email` varchar(50) DEFAULT NULL,
                                    `line1` varchar(50) DEFAULT NULL,
                                    `line2` varchar(50) DEFAULT NULL,
                                    `city` varchar(50) DEFAULT NULL,
                                    `province` varchar(50) DEFAULT NULL,
                                    `country` varchar(50) DEFAULT NULL,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `cart_item`</p>
                                <p>CREATE TABLE `cart` (
                                    `id` bigint(20) NOT NULL,
                                    `userId` bigint(20) DEFAULT NULL,
                                    `sessionId` varchar(100) NOT NULL,
                                    `token` varchar(100) NOT NULL,
                                    `status` smallint(6) NOT NULL DEFAULT 0,
                                    `firstName` varchar(50) DEFAULT NULL,
                                    `middleName` varchar(50) DEFAULT NULL,
                                    `lastName` varchar(50) DEFAULT NULL,
                                    `mobile` varchar(15) DEFAULT NULL,
                                    `email` varchar(50) DEFAULT NULL,
                                    `line1` varchar(50) DEFAULT NULL,
                                    `line2` varchar(50) DEFAULT NULL,
                                    `city` varchar(50) DEFAULT NULL,
                                    `province` varchar(50) DEFAULT NULL,
                                    `country` varchar(50) DEFAULT NULL,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `category`</p>
                                <p>CREATE TABLE `category` (
                                    `id` bigint(20) NOT NULL,
                                    `parentId` bigint(20) DEFAULT NULL,
                                    `title` varchar(75) NOT NULL,
                                    `metaTitle` varchar(100) DEFAULT NULL,
                                    `slug` varchar(100) NOT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `order`</p>
                                <p>CREATE TABLE `order` (
                                    `id` bigint(20) NOT NULL,
                                    `userId` bigint(20) DEFAULT NULL,
                                    `sessionId` varchar(100) NOT NULL,
                                    `token` varchar(100) NOT NULL,
                                    `status` smallint(6) NOT NULL DEFAULT 0,
                                    `subTotal` float NOT NULL DEFAULT 0,
                                    `itemDiscount` float NOT NULL DEFAULT 0,
                                    `tax` float NOT NULL DEFAULT 0,
                                    `shipping` float NOT NULL DEFAULT 0,
                                    `total` float NOT NULL DEFAULT 0,
                                    `promo` varchar(50) DEFAULT NULL,
                                    `discount` float NOT NULL DEFAULT 0,
                                    `grandTotal` float NOT NULL DEFAULT 0,
                                    `firstName` varchar(50) DEFAULT NULL,
                                    `middleName` varchar(50) DEFAULT NULL,
                                    `lastName` varchar(50) DEFAULT NULL,
                                    `mobile` varchar(15) DEFAULT NULL,
                                    `email` varchar(50) DEFAULT NULL,
                                    `line1` varchar(50) DEFAULT NULL,
                                    `line2` varchar(50) DEFAULT NULL,
                                    `city` varchar(50) DEFAULT NULL,
                                    `province` varchar(50) DEFAULT NULL,
                                    `country` varchar(50) DEFAULT NULL,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `order_item`</p>
                                <p>CREATE TABLE `order_item` (
                                    `id` bigint(20) NOT NULL,
                                    `productId` bigint(20) NOT NULL,
                                    `orderId` bigint(20) NOT NULL,
                                    `sku` varchar(100) NOT NULL,
                                    `price` float NOT NULL DEFAULT 0,
                                    `discount` float NOT NULL DEFAULT 0,
                                    `quantity` smallint(6) NOT NULL DEFAULT 0,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `product`</p>
                                <p>CREATE TABLE `product` (
                                    `id` bigint(20) NOT NULL,
                                    `userId` bigint(20) NOT NULL,
                                    `title` varchar(75) NOT NULL,
                                    `metaTitle` varchar(100) DEFAULT NULL,
                                    `slug` varchar(100) NOT NULL,
                                    `summary` tinytext DEFAULT NULL,
                                    `type` smallint(6) NOT NULL DEFAULT 0,
                                    `sku` varchar(100) NOT NULL,
                                    `price` float NOT NULL DEFAULT 0,
                                    `discount` float NOT NULL DEFAULT 0,
                                    `quantity` smallint(6) NOT NULL DEFAULT 0,
                                    `shop` tinyint(1) NOT NULL DEFAULT 0,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `publishedAt` datetime DEFAULT NULL,
                                    `startsAt` datetime DEFAULT NULL,
                                    `endsAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `product_category`</p>
                                <p>CREATE TABLE `product_category` (
                                    `productId` bigint(20) NOT NULL,
                                    `categoryId` bigint(20) NOT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `product_review`</p>
                                <p>CREATE TABLE `product_review` (
                                    `id` bigint(20) NOT NULL,
                                    `productId` bigint(20) NOT NULL,
                                    `parentId` bigint(20) DEFAULT NULL,
                                    `title` varchar(100) NOT NULL,
                                    `rating` smallint(6) NOT NULL DEFAULT 0,
                                    `published` tinyint(1) NOT NULL DEFAULT 0,
                                    `createdAt` datetime NOT NULL,
                                    `publishedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `transaction`</p>
                                <p>CREATE TABLE `transaction` (
                                    `id` bigint(20) NOT NULL,
                                    `userId` bigint(20) NOT NULL,
                                    `orderId` bigint(20) NOT NULL,
                                    `code` varchar(100) NOT NULL,
                                    `type` smallint(6) NOT NULL DEFAULT 0,
                                    `mode` smallint(6) NOT NULL DEFAULT 0,
                                    `status` smallint(6) NOT NULL DEFAULT 0,
                                    `createdAt` datetime NOT NULL,
                                    `updatedAt` datetime DEFAULT NULL,
                                    `content` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Table structure for table `user`</p>
                                <p>CREATE TABLE `user` (
                                    `id` bigint(20) NOT NULL,
                                    `firstName` varchar(50) DEFAULT NULL,
                                    `middleName` varchar(50) DEFAULT NULL,
                                    `lastName` varchar(50) DEFAULT NULL,
                                    `mobile` varchar(15) DEFAULT NULL,
                                    `email` varchar(50) DEFAULT NULL,
                                    `passwordHash` varchar(32) NOT NULL,
                                    `admin` tinyint(1) NOT NULL DEFAULT 0,
                                    `vendor` tinyint(1) NOT NULL DEFAULT 0,
                                    `registeredAt` datetime NOT NULL,
                                    `lastLogin` datetime DEFAULT NULL,
                                    `intro` tinytext DEFAULT NULL,
                                    `profile` text DEFAULT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</p>
                                <p>Indexes for dumped tables</p>
                                <p>Indexes for table `cart`</p>
                                <p>ALTER TABLE `cart`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_cart_user` (`userId`);</p>
                                <p>Indexes for table `cart_item`</p>
                                <p>ALTER TABLE `cart_item`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_cart_item_product` (`productId`),
                                    ADD KEY `idx_cart_item_cart` (`cartId`);</p>
                                <p>Indexes for table `category`</p>
                                <p>ALTER TABLE `category`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_category_parent` (`parentId`);</p>
                                <p>Indexes for table `order`</p>
                                <p>ALTER TABLE `order`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_order_user` (`userId`);</p>
                                <p>Indexes for table `order_item`</p>
                                <p>ALTER TABLE `order_item`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_order_item_product` (`productId`),
                                    ADD KEY `idx_order_item_order` (`orderId`);</p>
                                <p>Indexes for table `product`</p>
                                <p>ALTER TABLE `product`
                                    ADD PRIMARY KEY (`id`),
                                    ADD UNIQUE KEY `uq_slug` (`slug`),
                                    ADD KEY `idx_product_user` (`userId`);</p>
                                <p>Indexes for table `product_category`</p>
                                <p>ALTER TABLE `product_category`
                                    ADD PRIMARY KEY (`productId`,`categoryId`),
                                    ADD KEY `idx_pc_category` (`categoryId`),
                                    ADD KEY `idx_pc_product` (`productId`);</p>
                                <p>Indexes for table `product_review`</p>
                                <p>ALTER TABLE `product_review`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_review_product` (`productId`),
                                    ADD KEY `idx_review_parent` (`parentId`);</p>
                                <p>Indexes for table `transaction`</p>
                                <p>ALTER TABLE `transaction`
                                    ADD PRIMARY KEY (`id`),
                                    ADD KEY `idx_transaction_user` (`userId`),
                                    ADD KEY `idx_transaction_order` (`orderId`);</p>
                                <p>Indexes for table `user`</p>
                                <p>ALTER TABLE `user`
                                    ADD PRIMARY KEY (`id`),
                                    ADD UNIQUE KEY `uq_mobile` (`mobile`),
                                    ADD UNIQUE KEY `uq_email` (`email`);</p>
                                <p>AUTO_INCREMENT for dumped tables</p>
                                <p>AUTO_INCREMENT for table `cart`</p>
                                <p>ALTER TABLE `cart`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `cart_item`</p>
                                <p>ALTER TABLE `cart_item`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `category`</p>
                                <p>ALTER TABLE `category`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `order`</p>
                                <p>ALTER TABLE `order`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `order_item`</p>
                                <p>ALTER TABLE `order_item`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `product`</p>
                                <p>ALTER TABLE `product`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `product_review`</p>
                                <p>ALTER TABLE `product_review`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `transaction`</p>
                                <p>ALTER TABLE `transaction`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>AUTO_INCREMENT for table `user`</p>
                                <p>ALTER TABLE `user`
                                    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;</p>
                                <p>Constraints for dumped tables</p>
                                <p>Constraints for table `cart`</p>
                                <p>ALTER TABLE `cart`
                                    ADD CONSTRAINT `fk_cart_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `cart_item`</p>
                                <p>ALTER TABLE `cart_item`
                                    ADD CONSTRAINT `fk_cart_item_cart` FOREIGN KEY (`cartId`) REFERENCES `cart` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                    ADD CONSTRAINT `fk_cart_item_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `category`</p>
                                <p>ALTER TABLE `category`
                                    ADD CONSTRAINT `fk_category_parent` FOREIGN KEY (`parentId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `order`</p>
                                <p>ALTER TABLE `order`
                                    ADD CONSTRAINT `fk_order_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `order_item`</p>
                                <p>ALTER TABLE `order_item`
                                    ADD CONSTRAINT `fk_order_item_order` FOREIGN KEY (`orderId`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                    ADD CONSTRAINT `fk_order_item_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `product`</p>
                                <p>ALTER TABLE `product`
                                    ADD CONSTRAINT `fk_product_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `product_category`</p>
                                <p>ALTER TABLE `product_category`
                                    ADD CONSTRAINT `fk_pc_category` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                    ADD CONSTRAINT `fk_pc_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `product_review`</p>
                                <p>ALTER TABLE `product_review`
                                    ADD CONSTRAINT `fk_review_parent` FOREIGN KEY (`parentId`) REFERENCES `product_review` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                    ADD CONSTRAINT `fk_review_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;</p>
                                <p>Constraints for table `transaction`</p>
                                <p>ALTER TABLE `transaction`
                                    ADD CONSTRAINT `fk_transaction_order` FOREIGN KEY (`orderId`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                    ADD CONSTRAINT `fk_transaction_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                                    COMMIT;</p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script >

                                //--------------------------------------
                                function changePattern1(Pattern1) {
                                    if (Pattern1 == '') {
                                        alert('Please Enter Number');
                                    } else {
                                        $.post('<?php echo base_url('Developer_Exam/changePattern1') ?>', {Pattern1: Pattern1}, function (data) {
                                            $('#showPattern').empty();
                                            $('#showPattern').html(data);

                                        });
                                    }
                                }
                                //--------------------------------------
                                function changePattern2(Pattern2) {
                                    if (Pattern2 == '') {
                                        alert('Please Enter Number');
                                    } else {
                                        $.post('<?php echo base_url('Developer_Exam/changePattern2') ?>', {Pattern2: Pattern2}, function (data) {
                                            $('#showPattern2').empty();
                                            $('#showPattern2').html(data);

                                        });
                                    }
                                }
                                //--------------------------------------
                                function changePattern3(Pattern3) {
                                    if (Pattern3 == '') {
                                        alert('Please Enter Number');
                                    } else {
                                        $.post('<?php echo base_url('Developer_Exam/changePattern3') ?>', {Pattern3: Pattern3}, function (data) {
                                            $('#showPattern3').empty();
                                            $('#showPattern3').html(data);

                                        });
                                    }
                                }
                                //--------------------------------------
                                function datetothai(dateinput) {
                                    if (dateinput == '') {
                                        alert('Please Enter Date');
                                    } else {
                                        $.post('<?php echo base_url('Developer_Exam/datetothai') ?>', {dateinput: dateinput}, function (datethai) {
                                            $('#datethai').text(datethai);

                                        });
                                    }
                                }
                                //--------------------------------------
                                function searchdata(name) {
                                    if (name == '') {
                                        alert('Please Enter Name Suename');
                                    } else {
                                        var flickerAPI = "https://jsonplaceholder.typicode.com/users?";
                                        $.getJSON(flickerAPI, {
                                            name_like: name
                                        })
                                                .done(function (data) {
                                                    if (data.length > 0) {
                                                        $.each(data, function (i, field) {
                                                            //$("#showdata").empty();
                                                            $("#showdata").text(JSON.stringify(field) + " ");
                                                            return;

                                                        });
                                                    } else {
                                                        $("#showdata").text("No data");
                                                    }





                                                });

                                    }
                                }


        </script>
    </body>
</html>